const fs = require('fs');
const request = require('request');
const http = require('http');
const { Transform } = require("stream");
const express = require('express');
const app = express();

const sharp = require('sharp');
const azure = require("azure-storage");

const _ = require('lodash');

const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const randomID = require("random-id");

require('dotenv').load();

const getDimensions = (ratio) => {
  const dimensions = [];
  if (ratio === 1.78) {
    dimensions.push('930x620', '620x350', '595x334', '365x205', '310x175', '290x345', '230x129', '35x20');
  } else if (ratio === 1.6) {
    dimensions.push('930x410');
  } else if(ratio === 0.86) {
    dimensions.push('320x370', '145x250');
  } else if(ratio === 1) {
    dimensions.push('55x55');
  }
  return dimensions;
}

const downloadImage = (uri, filename, callback) => {
  request.head(uri, (err, res) => {
    if (err) {
      throw err;
    } else {
      request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    }
  });
}

const purgeImagesOnCdn = (cdnPath, imgContainer) => {
  const imagePath = `/${imgContainer}/${cdnPath}`;
  const postData = JSON.stringify({ imagePath : imagePath });
  let apiUrl = "cig-prod-api.azurewebsites.net";
  if (imgContainer === "cig-staging-images") apiUrl = "cig-staging-api.azurewebsites.net";
  const options = {
    headers: { 
      'Content-Type': 'application/json',
      'Content-Length': Buffer.byteLength(postData),
    },
    path: "/api/cdn/image/purge",
    method: "POST",
    host: apiUrl,
  };

  let apiResponseBuffer = "";
  const req = http.request(options, function(response) {
    response.setEncoding = ("utf8");
    response.pipe(new Transform({
        encoding: "utf8",
        decodeStrings: false,               
        highWaterMark: (16384 * 10),
        transform(chunk, encoding, callback) {
          apiResponseBuffer += chunk;
          this.push(chunk) && callback();
        },
        flush(callback) {
          try {
            console.log(JSON.parse(apiResponseBuffer));
          } catch (err) {
            console.log(err, "api-error");
          }
        }
    }));
  });
  req.write(postData);
  req.end();
  req.on('error', (error) => {
    console.error(error, "api purging error");
  });
} 

const blobService = azure.createBlobService();
const uploadToAzure = (newImage, cdnPath, imgContainer) => {
  blobService.createContainerIfNotExists(imgContainer, {
    publicAccessLevel: "blob"
  }, (error, result, response) => {
    if (!error) {
      if (result) {
        blobService.createBlockBlobFromLocalFile(imgContainer, cdnPath, newImage, (error, result, response) => {
          if (!error) {
            fs.unlinkSync(newImage);
            purgeImagesOnCdn(cdnPath, imgContainer);
            return "uploaded";
          } else {
            fs.unlinkSync(newImage);
            return "err";
          }
        });
      } else {
        fs.unlinkSync(newImage);
        return false;
      }
    } else {
      fs.unlinkSync(newImage);
    }
  });
};

const resizeImage = async (tempImage, newImage, cdnPath, width, height, imgContainer) => {
  console.log('resizing...')
  await sharp(tempImage)
  .resize(width, height)
  .jpeg()
  .rotate()
  .toBuffer()
  .then( data =>  {
    console.log('uploading resize...')
    fs.writeFileSync(newImage, data);
    uploadToAzure(newImage, cdnPath, imgContainer);
  })
  .catch( err => {
    console.log(err, 'exception in resizings');
    if (fs.existsSync(newImage)) {
      fs.unlinkSync(newImage);
    }
  });
}

const lqImage = async (tempImage, newImage, cdnPath, width, height, imgContainer) => {
  await sharp(tempImage)
  .resize(width, height)
  .jpeg({
    quality: 25,
  })
  .rotate()
  .blur(5)
  .toBuffer()
  .then( data =>  {
    console.log('uploading quality...')
    fs.writeFileSync(newImage, data);
    uploadToAzure(newImage, cdnPath, imgContainer);
  })
  .catch( err => {
    console.log(err, 'exception in quality');
    if (fs.existsSync(newImage)) {
      fs.unlinkSync(newImage);
    }
  });
}

app.post('/images/resize', (req, res) => {
  const body = _.get(req, 'body');
  const imagePath = _.get(body, 'imagePath');
  const articleCoverId = _.get(body, 'articleCoverId');
  const imgContainer = _.get(body, 'imgContainer');
  const aspectRatios = [1.78, 1.6, 0.86, 1];
  const imagesURL = [];
  for (let i=0; i<aspectRatios.length; i++) {
    imagesURL.push(`https://cricingif.blob.core.windows.net/${imgContainer}/article-images/${aspectRatios[i]}/${articleCoverId}.jpg`);
  }

  let count = 0, threads = 4;
  try {
    require('async').eachLimit(imagesURL, threads, function(url, next) {
      const tempImage = `${articleCoverId}-${aspectRatios[count]}`;
      downloadImage(url, tempImage, next);
      count++;
    }, async function() {
        console.log('Downloaded Successfully');
        for (let i=0; i<aspectRatios.length; i++) {
          const dimensions = getDimensions(aspectRatios[i]);
          const tempImage = `${articleCoverId}-${aspectRatios[i]}`;
          for (let i=0; i<dimensions.length; i++) {
            let width, height;
            width = parseInt(dimensions[i].split('x')[0], 10);
            height = parseInt(dimensions[i].split('x')[1], 10);
            const cdnPath = `article-images/reduce/${dimensions[i]}/${articleCoverId}.jpg`;
            const hqNewImage = `${articleCoverId}-${dimensions[i]}.jpg`;
            const lqNewImage = `${articleCoverId}-${dimensions[i]}-lq.jpg`;
            const lqCdnPath = `article-images/LQ/${dimensions[i]}/${articleCoverId}.jpg`;
            await lqImage(tempImage, lqNewImage, lqCdnPath, width, height, imgContainer);
            await resizeImage(tempImage, hqNewImage, cdnPath, width, height, imgContainer);
          }
          fs.unlinkSync(`${articleCoverId}-${aspectRatios[i]}`);
        }
        res.send('Done');
    })
  } catch (e) {
    res.send('Donwloading Images Failed');
  }
});

app.post('/team/images/resize', (req, res) => {
  const body = _.get(req, 'body');
  const imagePath = _.get(body, 'imagePath');
  const teamName = _.get(body, 'teamName');
  const imgContainer = _.get(body, 'imgContainer');

  const tempImage = `${teamName}-${randomID(10)}`;
  const teamNewImage = `${teamName}.png`;
  const cdnPath = `team-images/22x22/${teamName}.png`;
  console.log(`Downloading ${tempImage}...`);

  try {
    downloadImage(imagePath, tempImage, async () => {
      console.log('downloaded successfully...');
      await resizeImage(tempImage, teamNewImage, cdnPath, 22, 22, imgContainer);
      console.log(`Removing ${tempImage}...`);
      fs.unlinkSync(tempImage);
      res.send('Done');
    });
  } catch(e) {
    res.send('Failed');
  }
});

app.post('/blogger/images/resize', (req, res) => {
  const body = _.get(req, 'body');
  const imagePath = _.get(body, 'imagePath');
  const bloggerId = _.get(body, 'bloggerId');
  const imgContainer = _.get(body, 'imgContainer');

  const tempImage = `${bloggerId}-${randomID(10)}`;
  const newImage = `${bloggerId}.png`;
  const cdnPath = `blogger-images/50x50/${bloggerId}.png`;

  console.log(`Downloading ${tempImage}...`);
  try {
    downloadImage(imagePath, tempImage, async () => {
      console.log('downloaded successfully...');
      await resizeImage(tempImage, newImage, cdnPath, 50, 50, imgContainer);
      console.log(`Removing ${tempImage}...`);
      fs.unlinkSync(tempImage);
      res.send('Done');
    });
  } catch(e) {
    res.send('Failed');
  }
});


app.post('/upload', (req, res) => {
  console.log('hello');
  const cdnPath = `css/main.css`;
  const containerName = 'test-container';
  blobService.createBlockBlobFromLocalFile(containerName, cdnPath, 'main.css', (error, result, response) => {
    if (!error) {
      console.log("uploaded")
      res.send('done');
      return "uploaded";
    } else {
      return "err";
    }
  });
});

const port = process.env.PORT || 8080;
// const server = app.listen(port, () => {
//    console.log(server.address(), "server");
//    const host = server.address().address
//    console.log(host, "host");
//    const port = server.address().port
//    console.log("Example app listening at http://%s:%s", "localhost", port)
// });
app.listen(port, (req, res) => {
  console.log(("App is running at http://localhost:%d"), port);
  console.log("Press CTRL-C to stop\n");
});